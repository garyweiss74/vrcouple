define([
    'text!resources/ivd/Inserts1.ivd',
    'text!resources/ivd/Inserts2.ivd',
    'text!resources/ivd/Inserts3.ivd',
    'text!resources/ivd/Inserts4.ivd',
    'text!resources/ivd/Inserts5.ivd',
    'text!resources/ivd/BG.ivd'
],
function(insert1, insert2, insert3, insert4, insert5, BG) {
    var ivds = {
        insert1:insert1,
        insert2:insert2,
        insert3:insert3,
        insert4:insert4,
        insert5:insert5,
        BG:BG
    };

    return ivds;
});
