define(['resources/ivds'], function(ivds) {

    var nodes = [
    	{
    		id: 'vrcouple::insert1',
    		source: ivds.insert1
    	},
        {
    		id: 'vrcouple::insert2',
    		source: ivds.insert2
    	},
        {
    		id: 'vrcouple::insert3',
    		source: ivds.insert3
    	},
        {
    		id: 'vrcouple::insert4',
    		source: ivds.insert4
    	},
        {
    		id: 'vrcouple::insert5',
    		source: ivds.insert5
    	},
        {
    		id: 'vrcouple::BG',
    		source: ivds.BG
    	}
    ];

    return nodes;
});
