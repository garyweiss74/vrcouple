define([
        'resources/ivds',
        'resources/nodes',
        'text!config/settings.json',
        'text!resources/skins.json',
        'resources/skins',
        'js/overlays'
    ],
    function (ivds, nodes, settings, skinsJSON, skinsJS, overlays) {

        'use strict';

        // locals
        var player;
        var interlude;
        var cameraCenter;
        var head = 'vrcouple::BG';
        settings = JSON.parse(settings);
        var MIN_SWITCH_OUT = -0.9;
        var MAX_SWITCH_OUT = -0.4;
        var MIN_SWITCH_IN = -0.3;
        var MAX_SWITCH_IN = -1.0;

        // init
        function onInit(interludeRef) {
            player = interludeRef.player;
            interlude = interludeRef;
            cameraCenter = player.vr.vec3(-0.4338, 0, 0.9007);

            // nodes
            player.repository.add(nodes);

            // setup vr
            player.vr.setup({
                controls: "auto", // auto-detect dev
                vrEnabled: false, // in the land of the blind ...
                initializeCanvas: true,
                camera: {
                    direction: cameraCenter,
                    fov: 35, // wider -> narrower  (10..100)
                    aspect: window.innerWidth / window.innerHeight,
                    near: 0.1,
                    far: 1000
                },
                renderer: {
                    width: window.innerWidth,
                    height: window.innerHeight
                },
                sphere360: {},

                // TODO: do we really need this light?
                light: {
                    position: player.vr.vec3(2, 2, 2)
                }
            });

            function appendNextInsertBit() {
                if (state == 'out') {
                    if (currentInsertBit < MAX_INSERT_BIT) {
                        currentInsertBit++;
                        console.log('**** playing bit ' + currentInsertBit + ' ****');
                        player.playlist.push("vrcouple::insert" + currentInsertBit);
                    } else {
                        console.log('**** SWITCHING BACK IN ****');
                        state = 'in';
                        player.playlist.push("vrcouple::BG");
                        player.currentTime = player.currentTime + player.currentNodeRemainingTime + currentBGTime;
                    }
                }
            }

            player.on(player.events.playlistcritical, appendNextInsertBit);

            // listen to draw events
            var counter = 0;
            var state = 'in';
            var currentInsertBit = 0;
            var currentBGTime = 0;
            var MAX_INSERT_BIT = 5;
            player.on('vr.frame', function (frameState) {
                counter++;

                var directionOnXZPlane = frameState.projectOnPlane(player.vr.vec3(0, 1, 0));
                var quaternion = (new THREE.Quaternion()).setFromUnitVectors(cameraCenter, directionOnXZPlane);
                var euler = (new THREE.Euler()).setFromQuaternion(quaternion);
                var angle = euler.y;
                if (counter % 30 === 0) {
                    console.log('angle = ' + angle);
                }

                if (state === 'in' &&
                    (currentInsertBit < MAX_INSERT_BIT) &&
                    (angle < MAX_SWITCH_OUT && angle > MIN_SWITCH_OUT)) {
                    currentBGTime = player.currentNodeTime;
                    console.log('**** SWITCHING OUT **** @' + currentBGTime);
                    state = 'out';

                    currentInsertBit++;
                    console.log('**** playing bit ' + currentInsertBit + ' ****');
                    player.seek("vrcouple::insert" + currentInsertBit);
                }

                if (state === 'out' &&
                    (angle < MAX_SWITCH_IN || angle > MIN_SWITCH_IN)) {
                    console.log('**** SWITCHING BACK IN ****');
                    state = 'in';
                    player.playlist.push("vrcouple::BG");
                    player.currentTime = player.currentTime + player.currentNodeRemainingTime + currentBGTime;
                }

            });

            // wait a bit before you react. TODO: there must be a better way to start VR
            setTimeout(function () {
                player.vr.start();
            }, 1000);
        }

        // project essentials
        return {
            settings: settings,
            onInit: onInit,
            head: head,
            onStarted: function () {},
            onEnd: function () {}
        };
    });
