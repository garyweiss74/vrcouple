require.config({
    paths: {
        'underscore': 'https://cdnjs.cloudflare.com/ajax/libs/underscore.js/1.8.3/underscore-min',
        'text': 'https://cdnjs.cloudflare.com/ajax/libs/require-text/2.0.12/text.min',
        'InterludePlayer': 'https://d3425luerwqydx.cloudfront.net/players/htmlplayer/release/4.4.0/30/player.min.gz',
        'InterludePlayerPlugins': 'https://d3425luerwqydx.cloudfront.net/players/htmlplayer/release/4.4.0/30/plugins.min.gz',
        'when': 'https://cdnjs.cloudflare.com/ajax/libs/when/2.7.1/when',
        'vr': 'js/lib/vr.min'
    },
    shim: {
        'InterludePlayerPlugins': {
            deps: ['InterludePlayer']
        },
        'vr': {
            deps: ['InterludePlayer']
        }
    },
    waitSeconds: 100
});

require([
    'flowController',
    'utils',
    'when',
    'text!config/playerOptions.json',
    'InterludePlayer',
    'InterludePlayerPlugins',
    'vr',
    'app.min'
],
function(FlowController, utils, when, playerOptions, InterludePlayer, InterludePlayerPlugins, VR, app) {
    'use strict';
    var interlude = {};
    utils.sendGaEvent('player', 'player-loaded');
    var playerLoadedTime = new Date();

    if (!utils.checkEnvironment(InterludePlayer)) {
        window.addEventListener('message', utils.handleFallbackMessage);
        return;
    }

    // create a player and add it to the "interlude" global anchor
    interlude.player = window.player = new InterludePlayer('#player', JSON.parse(playerOptions));

    //when player ready check if has  window.INTERLUDE_PLAYER_READY and it function then apply with the interlude player - used for VAST and VPAID
    if(window.INTERLUDE_PLAYER_READY && typeof window.INTERLUDE_PLAYER_READY === 'function') {
        window.INTERLUDE_PLAYER_READY.apply(null, [interlude.player]);
    }

    // when the gui is ready, add the components
    interlude.player.gui.promise.done(function() {

        var flowController = new FlowController(interlude, app);

        // Start
        if (window.debug) {
            addListeners();
            interlude.player.initPlugin('debug', {});
        }

        var queryParams = window.InterludePlayer.environment.queryParams;
        if (queryParams.embedapi === 'true') {
            interlude.player.initPlugin('embedBridge', {
                uid: queryParams.embedapiuid,
                appObj: app
            });
        }

        var d = when.defer();
        app.onInit(interlude, function (err) {
            if (err) {
                // TODO: what should we do in this case?
                console.log('project initialization failed.');
                throw 'project onInit() failed';
            }
            d.resolve();
        });

        // synchronous init can resolve now
        if (!app.settings.asyncInit) {
            d.resolve();
        }
        d.promise.then(function () {

            // track performance
            if (window.InterludeAnalytics) {
                var trackFunc = window.InterludeAnalytics._track_?window.InterludeAnalytics._track_:window.InterludeAnalytics.track;
                trackFunc('in:perfTimingL0', { appReady:(new Date()).getTime(), playerLoaded:playerLoadedTime.getTime() });
            }

            flowController.init();
            interlude.player.append((typeof app.head === 'function') ? app.head() : app.head);

            if (app.settings.autoPlay || InterludePlayer.environment.queryParams.autoPlay || InterludePlayer.environment.queryParams.autoplay || InterludePlayer.environment.queryParams.auto_play) {
                interlude.player.play();
            }
        });
    });

    if (interlude.player.videoTech() === 'hls') {
        utils.refreshHLS();
    }
}

);
